package com.google.android.glass.sample.apidemo.card;

import java.util.Calendar;

/**
 * Created by graeme on 01/10/14.
 */

public class Note {
     String title;
    String body;
    Calendar cal;
    public Note(){
           String title = "Title";
           String body = "Body";
           Calendar cal = Calendar.getInstance();
    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Calendar getCal() {
        return cal;
    }

    public void setCal(Calendar cal) {
        this.cal = cal;
    }
}
