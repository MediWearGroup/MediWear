package com.google.android.glass.sample.apidemo.voicemenu;

import android.app.Activity;
import android.content.Intent;
import android.speech.RecognizerIntent;


import java.util.List;

/**
 * Created by graeme on 01/10/14.
 */
public class SpeechToText extends Activity
{
    private static final int SPEECH_REQUEST = 1;
    private static final int RESULT_OK = -1;
    private String spokenText = "";

    //Supposedly recognizes the text
    private void displaySpeechRecognizer()
    {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        startActivityForResult(intent, SPEECH_REQUEST);
    }

    @Override
    //Supposedly returns the speech
    protected void onActivityResult(int requestCode, int resultCode,Intent data)
    {
        if (requestCode == SPEECH_REQUEST && resultCode == RESULT_OK) {
            List<String> results = data.getStringArrayListExtra(
                    RecognizerIntent.EXTRA_RESULTS);
            String spokenText = results.get(0);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
