/**
 * Created by user on 2014-09-30.
 */

import java.lang.String;
import java.util.Arrays;
import java.security.MessageDigest;

import javax.crypto.*;
/**
 * import javax.crypto.Cipher;
 * import javax.crypto.SecretKey;
 * import javax.crypto.spec.*;
*/

public class SHA
{

    private final String SALT = "taPI3QZsOCbM#4FOC0NSplv&H"; // random

    // testing with sample data
    public void main(String[] args)
    {
        byte[] happy = crypt("bob","2","fat");
        String sad = decrypt("bob", "2", happy);
        System.out.println(sad);
    }

    // encrypt the inputted message
    public byte[] crypt(String username, String password, String message)
    {
        SecretKey superKey = makeKey(username,password);

        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, superKey);                 // initialise cipher to with secret key

        // encrypt message
        byte[] encrypted = cipher.doFinal(message.getBytes());
        return encrypted;
    }

    public String decrypt(String username, String password, byte[] encrypted)
    {

        SecretKey superKey = makeKey(username,password);

        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, superKey);                 // initialise cipher to with secret key

        // decrypt message
        byte[] decrypted = cipher.doFinal(encrypted);
        String output = new String(decrypted);
        return output;
    }

    private SecretKey makeKey(String username, String password)
    {
        // generate a hash to make a secret key to create cipher
        byte[] key = (SALT + username + password).getBytes("UTF-8");

        MessageDigest sha = MessageDigest.getInstance("SHA-1");     // generate hash
        key = sha.digest(key);
        key = Arrays.copyOf(key, 16);                               // use only first 128 bit

        System.out.println(key);

        SecretKey superKey = new SecretKeySpec(key, "AES");     // it's better than just the key
        return superKey;
    }
}
