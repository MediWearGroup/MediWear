MediWear
========

Google Glasses interface for the Telus health database

This project was meant for the Telus Google Glass challenge at McMaster University on October 2014

> **WARNING**: this repository has been abandoned. The team could not figure out how to work with Gradle. We could not figure out how to manage the cards system. We also had limited access to the device before the deadline.